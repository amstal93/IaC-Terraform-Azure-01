resource "azurerm_storage_account" "terra_storage" {
  name 			= "netconfterraform"
  resource_group_name 	= "${azurerm_resource_group.terraform_resource_group.name}"
  location 		= "${var.location}"
  account_tier             = "Standard"
  account_replication_type = "LRS"

  tags {
	group = "DemoTest"
  }
}

resource "azurerm_storage_container" "la_cont" {
  name 			= "vhds"
  resource_group_name 	= "${azurerm_resource_group.terraform_resource_group.name}"
  storage_account_name 	= "${azurerm_storage_account.terra_storage.name}"
  container_access_type = "private"
}
